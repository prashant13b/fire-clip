package xyz.prashant13b.fire_clip

import android.annotation.TargetApi
import android.content.ClipboardManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log

import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {
 

  @TargetApi(Build.VERSION_CODES.HONEYCOMB)
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    GeneratedPluginRegistrant.registerWith(this)
    val clipboard = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
      getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    } else null
      var savedClip = clipboard?.primaryClip.toString()
    MethodChannel(flutterView, "my.channel").setMethodCallHandler { call, result ->
      if (call.method == "getBatteryLevel") {
        
      } else {
        result.notImplemented()
      }
    }
   clipboard?.addPrimaryClipChangedListener {
       val currentClip = clipboard.primaryClip.toString()
    //   Log.d("Clip", "$currentClip , $savedClip")
    if(savedClip != currentClip) {
        savedClip = currentClip
        MethodChannel(flutterView, "my.channel").invokeMethod("foo", clipboard.primaryClip.toString())
    }
    }

    
  
  }  
}
