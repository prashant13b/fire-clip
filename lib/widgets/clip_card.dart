import 'package:flutter/material.dart';

class ClipCard extends StatelessWidget {
  final _clip;
  const ClipCard(this._clip);
  
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(color: Colors.white,
      child: Center(
        child: Text(_clip),
      ),
      )
    );
  }
}