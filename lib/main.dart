
import 'package:fire_clip/home_page.dart';
import 'package:flutter/material.dart';
import 'sign_in.dart';
import 'login_page.dart';

void main() => runApp(MyApp());
//final SignIn signIn = SignIn();
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Login',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Init(),
    );
  }
}

class Init extends StatelessWidget {
  const Init({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (signIn.isInit && signIn.fireUser!=null) return HomePage();
    else return LoginPage();
  }
}