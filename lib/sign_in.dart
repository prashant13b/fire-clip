import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SignIn{
  final Firestore _db = Firestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  FirebaseUser fireUser;
  var isInit = false;
String name;
String email;
String imageUrl;
String uuid;

SignIn() {
  init();
}
init() async{
  fireUser = await _auth.currentUser();
  isInit = true;
}
Future<FirebaseUser> getUser() async {
  return await _auth.currentUser();
}

Future<String> signInWithGoogle() async {
final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
final GoogleSignInAuthentication googleSignInAuthentication =
await googleSignInAccount.authentication;

final AuthCredential credential = GoogleAuthProvider.getCredential(
accessToken: googleSignInAuthentication.accessToken,
idToken: googleSignInAuthentication.idToken,
);

final FirebaseUser user = await _auth.signInWithCredential(credential);

// Checking if email and name is null
assert(user.email != null);
assert(user.displayName != null);
assert(user.photoUrl != null);
assert(user.uid != null);

name = user.displayName;
email = user.email;
imageUrl = user.photoUrl;
uuid = user.uid;

// Only taking the first part of the name, i.e., First Name
if (name.contains(" ")) {
name = name.substring(0, name.indexOf(" "));
}

assert(!user.isAnonymous);
assert(await user.getIdToken() != null);

final FirebaseUser currentUser = await _auth.currentUser();
assert(user.uid == currentUser.uid);

return 'signInWithGoogle succeeded: $user';
}

void signOutGoogle() async{
await googleSignIn.signOut();

print("User Sign Out");
}
}

final SignIn signIn = SignIn();