import 'package:fire_clip/widgets/clip_card.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'login_page.dart';
import 'sign_in.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);
  final String title = "Fire Clip";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final Firestore _db = Firestore.instance;
  @override
  void initState() {
    super.initState();
    platform.setMethodCallHandler(nativeMethodCallHandler);
  }

  static const platform = const MethodChannel(
      'my.channel');

  @override
  Widget build(BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title:  Text(widget.title),
            actions: <Widget>[
              IconButton(icon: new Icon(Icons.vpn_key), onPressed: () { signIn.signOutGoogle(); Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (context) => LoginPage()));})
            ],
          ),
          body: Center(
              child: StreamBuilder<QuerySnapshot>(
                  stream: _db.collection('users').document(signIn.fireUser.uid).collection("data").snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.hasError)
                      return new Text('Error: ${snapshot.error}');
                    switch (snapshot.connectionState) {
                      case ConnectionState.waiting:
                        return new Text('Loading...');
                      default:
//                        var list = new List();
//                        snapshot.data.documents.forEach((f) => {
//                          list.add(f.)
//                        });
                        return new ListView.builder(itemCount: snapshot.data.documents.length,
                            itemBuilder : (context,i){
                                final text = snapshot.data.documents[i]['clip'].toString();
                                return new FlatButton(
                                  child: ClipCard(text),
                                  onPressed: () => copyText(text),
                                );

                            });
                    }
                  })
          ),
        );

    // This trailing comma makes auto-formatting nicer for build methods.
  }
  setData() async{
    var list = new List();
    final data = await getData(Clipboard.kTextPlain);
    final snapshot = await _db.collection('users').document(signIn.fireUser.uid).collection('data').getDocuments();
    snapshot.documents.forEach((f)=>{
      list.add(f['clip'].toString())
    });
    print(data.text);

    if(!list.contains(data.text)) {
      _db.collection('users').document(signIn.fireUser.uid)
          .collection("data")
          .add({"clip": data.text});
    }
  }
  copyText(copyText) {
    Clipboard.setData(ClipboardData(text: copyText));
  }

  static Future<ClipboardData> getData(String format) async {
    final Map<String, dynamic> result = await SystemChannels.platform.invokeMethod(
      'Clipboard.getData',
      format,
    );
    if (result == null)
      return null;

    return ClipboardData(text: result['text']);
  }
  Future<dynamic> nativeMethodCallHandler(MethodCall methodCall) async {
    switch (methodCall.method) {
      case 'foo':
        return setData();
      case 'bar':
        return 123.0;
      default:
    }

  }
//void getList() async{
//_text.length==0 ? Text('no data') :
//  new ListView.builder(
//  itemCount: _text.length,
//  itemBuilder: (context,i){
//  final text = _text[i];
//  return new FlatButton(
//  child: ClipCard(text),
//  onPressed: () => copyText(text),
//  );
//  },
//  )
//}
}